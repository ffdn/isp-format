
import os

path = os.path.dirname(__file__)

versions = {
    0.1: os.path.join(path, '0.1', 'isp-schema-spec.rst'),
    0.2: os.path.join(path, '0.2', 'isp-schema-spec.rst')
}
